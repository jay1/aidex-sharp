-- 菜单 SQL
insert into sys_menu (id, menu_code, menu_name, sort, path, component, is_frame, is_cache, menu_type, visible, perms, icon, parent_id, parent_ids, tree_sort, tree_sorts, tree_level, tree_leaf, status, create_by, create_time, update_by, update_time, create_dept, update_ip, version, remark, del_flag)
values('231b39cc631b42649fecb5cc979e614a', 'appSlide', '幻灯片', '50', 'appSlide', 'app/appslide/index', 1, 0, 'C', 0, 'app:appSlide:list', '#', '3', '3/231b39cc631b42649fecb5cc979e614a', '50', '000030/50', '2', 'n', '0', '1', sysdate(), '1', sysdate(), '', '127.0.0.1', 0, '', '0');

-- 按钮 SQL
insert into sys_menu (id, menu_code, menu_name, sort, path, component, is_frame, is_cache, menu_type, visible, perms, icon, parent_id, parent_ids, tree_sort, tree_sorts, tree_level, tree_leaf, status, create_by, create_time, update_by, update_time, create_dept, update_ip, version, remark, del_flag)
values('e3406306523f40fcaebd4432fa1cdc32', 'appSlideQuery', '幻灯片查询', '10', '#', '', 1, 0, 'F', 0, 'app:appSlide:query', '#', '231b39cc631b42649fecb5cc979e614a', '3/231b39cc631b42649fecb5cc979e614a/e3406306523f40fcaebd4432fa1cdc32', '10', '000030/50/10', '3', 'y', '0', '1', sysdate(), '1', sysdate(), '', '127.0.0.1', 0, '', '0');

insert into sys_menu (id, menu_code, menu_name, sort, path, component, is_frame, is_cache, menu_type, visible, perms, icon, parent_id, parent_ids, tree_sort, tree_sorts, tree_level, tree_leaf, status, create_by, create_time, update_by, update_time, create_dept, update_ip, version, remark, del_flag)
values('0cee849e733247d4bffd99762f7a6d5f', 'appSlideAdd', '幻灯片新增', '20', '#', '', 1, 0, 'F', 0, 'app:appSlide:add', '#', '231b39cc631b42649fecb5cc979e614a', '3/231b39cc631b42649fecb5cc979e614a/0cee849e733247d4bffd99762f7a6d5f', '20', '000030/50/20', '3', 'y', '0', '1', sysdate(), '1', sysdate(), '', '127.0.0.1', 0, '', '0');

insert into sys_menu (id, menu_code, menu_name, sort, path, component, is_frame, is_cache, menu_type, visible, perms, icon, parent_id, parent_ids, tree_sort, tree_sorts, tree_level, tree_leaf, status, create_by, create_time, update_by, update_time, create_dept, update_ip, version, remark, del_flag)
values('f1133b116a454fc28d2a1e0a40324600', 'appSlideEdit', '幻灯片修改', '30', '#', '', 1, 0, 'F', 0, 'app:appSlide:edit', '#', '231b39cc631b42649fecb5cc979e614a', '3/231b39cc631b42649fecb5cc979e614a/f1133b116a454fc28d2a1e0a40324600', '30', '000030/50/30', '3', 'y', '0', '1', sysdate(), '1', sysdate(), '', '127.0.0.1', 0, '', '0');

insert into sys_menu (id, menu_code, menu_name, sort, path, component, is_frame, is_cache, menu_type, visible, perms, icon, parent_id, parent_ids, tree_sort, tree_sorts, tree_level, tree_leaf, status, create_by, create_time, update_by, update_time, create_dept, update_ip, version, remark, del_flag)
values('ede212135f8b447294b9827a42b58118', 'appSlideRemove', '幻灯片删除', '10', '#', '', 1, 0, 'F', 0, 'app:appSlide:remove', '#', '231b39cc631b42649fecb5cc979e614a', '3/231b39cc631b42649fecb5cc979e614a/ede212135f8b447294b9827a42b58118', '40', '000030/50/40', '3', 'y', '0', '1', sysdate(), '1', sysdate(), '', '127.0.0.1', 0, '', '0');

insert into sys_menu (id, menu_code, menu_name, sort, path, component, is_frame, is_cache, menu_type, visible, perms, icon, parent_id, parent_ids, tree_sort, tree_sorts, tree_level, tree_leaf, status, create_by, create_time, update_by, update_time, create_dept, update_ip, version, remark, del_flag)
values('550a9482c5e149ea860f28596d42ca67', 'appSlideExport', '幻灯片查询', '50', '#', '', 1, 0, 'F', 0, 'app:appSlide:export', '#', '231b39cc631b42649fecb5cc979e614a', '3/231b39cc631b42649fecb5cc979e614a/550a9482c5e149ea860f28596d42ca67', '50', '000030/50/50', '3', 'y', '0', '1', sysdate(), '1', sysdate(), '', '127.0.0.1', 0, '', '0');

