package  com.aidex.app.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.aidex.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.aidex.common.utils.log.annotation.FieldRemark;
import com.aidex.common.utils.log.annotation.LogField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.aidex.common.annotation.Excel;
/**
 * 幻灯片对象 app_slide
 * @author wws
 * @email 595706397@qq.com
 * @date 2021-12-20
 */
@Data
public class AppSlide extends BaseEntity<AppSlide>
{
    private static final long serialVersionUID = 1L;

    /** 幻灯片地址 */
    @Excel(name = "幻灯片地址")
    @NotBlank(message = "幻灯片地址不允许为空")
    private String img;

    /** 平台 */
    @Excel(name = "平台", dictType = "app_platform")
    @NotBlank(message = "平台不允许为空")
    private String platform;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sort;

    /** 状态（0显示 1隐藏） */
    @Excel(name = "状态", dictType = "sys_is_valid")
    private String status;

    public void setImg(String img) 
    {
        this.img = img;
    }

    public String getImg() 
    {
        return img;
    }

    public void setPlatform(String platform) 
    {
        this.platform = platform;
    }

    public String getPlatform() 
    {
        return platform;
    }

    public void setSort(Integer sort) 
    {
        this.sort = sort;
    }

    public Integer getSort() 
    {
        return sort;
    }

    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("img", getImg())
            .append("platform", getPlatform())
            .append("sort", getSort())
            .append("status", getStatus())
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
