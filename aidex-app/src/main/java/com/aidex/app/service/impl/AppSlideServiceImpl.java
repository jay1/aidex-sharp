package  com.aidex.app.service.impl;

import java.util.List;
import com.github.pagehelper.PageInfo;
import com.aidex.common.core.domain.entity.SysUser;
import com.aidex.common.core.domain.entity.SysDept;
import com.aidex.common.core.service.BaseServiceImpl;
import com.aidex.framework.cache.DeptUtils;
import com.aidex.framework.cache.UserUtils;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import  com.aidex.app.mapper.AppSlideMapper;
import  com.aidex.app.domain.AppSlide;
import  com.aidex.app.service.AppSlideService;
import org.springframework.transaction.annotation.Transactional;
import com.aidex.common.utils.NumberUtils;

/**
 * 幻灯片Service业务层处理
 * @author wws
 * @email 595706397@qq.com
 * @date 2021-12-20
 */
@Service
@Transactional(readOnly = true)
public class AppSlideServiceImpl extends BaseServiceImpl<AppSlideMapper, AppSlide> implements AppSlideService
{
    private static final Logger log = LoggerFactory.getLogger(AppSlideServiceImpl.class);

    /**
     * 获取单条数据
     * @param appSlide 幻灯片
     * @return 幻灯片
     */
    @Override
    public AppSlide get(AppSlide appSlide)
    {
        AppSlide dto = super.get(appSlide);
        return dto;
    }

    /**
     * 获取单条数据
     * @param id 幻灯片id
     * @return 幻灯片
     */
    @Override
    public AppSlide get(String id)
    {
        AppSlide dto = super.get(id);
        return dto;
    }


    /**
     * 查询幻灯片列表
     * @param appSlide 幻灯片
     * @return 幻灯片
     */
    @Override
    public List<AppSlide> findList(AppSlide appSlide)
    {
        return super.findList(appSlide);
    }

    /**
     * 分页查询幻灯片列表
     * @param appSlide 幻灯片
     * @return 幻灯片
     */
    @Override
    public PageInfo<AppSlide> findPage(AppSlide appSlide)
    {
        return super.findPage(appSlide);
    }

    /**
     * 保存幻灯片
     * @param appSlide
     * @return 结果
     */
    @Override
    public boolean save(AppSlide appSlide)
    {
        return super.save(appSlide);
    }


    /**
     * 删除幻灯片信息
     * @param appSlide
     * @return 结果
     */
    @Override
    public boolean remove(AppSlide appSlide)
    {
        return super.remove(appSlide);
    }

    /**
     * 批量删除幻灯片
     * @param ids 需要删除的幻灯片ID
     * @return 结果
     */
    @Transactional(readOnly = false)
    @Override
    public int deleteAppSlideByIds(String[] ids)
    {
        return mapper.deleteAppSlideByIds(ids);
    }

    /**
     * 获取最大编号
     * @param appSlide 幻灯片
     * @return 结果
     */
    @Override
    public int findMaxSort(AppSlide appSlide)
    {
        return NumberUtils.nextOrder(mapper.findMaxSort(appSlide));
    }
}
