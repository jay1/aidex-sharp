package  com.aidex.app.service;

import com.aidex.common.core.service.BaseService;
import  com.aidex.app.domain.AppSlide;

/**
 * 幻灯片Service接口
 * @author wws
 * @email 595706397@qq.com
 * @date 2021-12-20
 */
public interface AppSlideService extends BaseService<AppSlide>
{

    /**
     * 批量删除幻灯片
     * @param ids 需要删除的幻灯片ID集合
     * @return 结果
     */
    public int deleteAppSlideByIds(String[] ids);

    /**
    * 获取最大编号
    * @param appSlide 幻灯片
    * @return 结果
    */
    public int findMaxSort(AppSlide appSlide);

}
