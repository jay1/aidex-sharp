package  com.aidex.app.mapper;

import com.aidex.common.core.mapper.BaseMapper;
import  com.aidex.app.domain.AppSlide;

/**
 * 幻灯片Mapper接口
 * @author wws
 * @email 595706397@qq.com
 * @date 2021-12-20
 */
public interface AppSlideMapper extends BaseMapper<AppSlide>
{

    /**
     * 批量删除幻灯片
     * @param ids 需要删除的幻灯片ID集合
     * @return
     */
    public int deleteAppSlideByIds(String[] ids);

    /**
     * 获取最大编号
     * @param appSlide 幻灯片
     * @return 结果
     */
    public Integer findMaxSort(AppSlide appSlide);

}
