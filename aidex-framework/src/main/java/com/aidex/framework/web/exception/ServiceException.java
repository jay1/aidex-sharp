package com.aidex.framework.web.exception;

public class ServiceException extends RuntimeException {

	/**
	 * spring事物只接受运行期异常回滚
	 */
	private static final long serialVersionUID = 1L;

	public ServiceException() {                    //构造详细信息为null的异常
	}

	public ServiceException(String message) {            //构造带指定详细信息的新异常
		super(message);
	}

	public ServiceException(String message, Throwable cause) {    //构造带指定详细信息和原因的新异常
		super(message, cause);
	}

	public ServiceException(Throwable cause) {                    //根据指定的原因和 (cause==null ? null : cause.toString()) 的详细消息构造新异常
		super(cause);
	}
}
