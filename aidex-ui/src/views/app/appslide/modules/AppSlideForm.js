import AntModal from '@/components/pt/dialog/AntModal'
import { getAppSlide, addAppSlide, updateAppSlide, findMaxSort } from '@/api/app/appSlide'

export default {
  name: 'CreateForm',
  props: {
    platformOptions: {
      type: Array,
      required: true
    },
    statusOptions: {
      type: Array,
      required: true
    } },
  components: {
    AntModal },
  data () {
    return {
      open: false,
      spinning: false,
      delayTime: 100,
      labelCol: { span: 4 },
      wrapperCol: { span: 14 },
      loading: false,
      total: 0,
      id: undefined,
      formTitle: '添加幻灯片',
      // 表单参数
      form: {},
      rules: {
        img: [{ required: true, message: '幻灯片地址不能为空', trigger: 'blur' }],
        platform: [{ required: true, message: '平台不能为空', trigger: 'blur' }]
      }
    }
  },
  filters: {},
  created () {},
  computed: {},
  watch: {},
  mounted () {},
  methods: {
    onClose () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 取消按钮
    cancel () {
      this.open = false
      this.reset()
      this.$emit('close')
    },
    // 表单重置
    reset () {
      this.form = {
        id: undefined,
        img: undefined,
        platform: undefined,
        sort: undefined,
        status: undefined
      }
    },
    /** 新增按钮操作 */
    handleAdd () {
      this.reset()
      this.open = true
      /** 获取最大编号 */
      findMaxSort().then(response => {
        this.form.sort = response.data
        this.formTitle = '添加幻灯片'
      })
    },
    /** 修改按钮操作 */
    handleUpdate (row) {
      this.reset()
      this.open = true
      this.spinning = !this.spinning
      const appSlideId = row.id
      getAppSlide(appSlideId).then(response => {
        this.form = response.data
        this.formTitle = '修改幻灯片'
        this.spinning = !this.spinning
      })
    },
    /** 提交按钮 */
    submitForm: function () {
      this.$refs.form.validate(valid => {
        if (valid) {
            const saveForm = JSON.parse(JSON.stringify(this.form))
			if (this.form.id !== undefined) {
				updateAppSlide(saveForm).then(response => {
					this.$message.success('新增成功', 3)
					this.open = false
					this.$emit('ok')
					this.$emit('close')
				})
              } else {
				addAppSlide(saveForm).then(response => {
					this.$message.success('新增成功', 3)
					this.open = false
					this.$emit('ok')
					this.$emit('close')
				})
			}
        } else {
          return false
        }
      })
    },
    back () {
      const index = '/app/appslide/index'
      this.$router.push(index)
    }
  }
}
