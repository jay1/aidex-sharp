import request from '@/utils/request'

// 查询幻灯片列表
export function listAppSlide (query) {
  return request({
    url: '/app/appSlide/list',
    method: 'get',
    params: query
  })
}

// 查询幻灯片详细
export function getAppSlide (id) {
  return request({
    url: '/app/appSlide/' + id,
    method: 'get'
  })
}

// 新增幻灯片
export function addAppSlide (data) {
  return request({
    url: '/app/appSlide',
    method: 'post',
    data: data
  })
}

// 修改幻灯片
export function updateAppSlide (data) {
  return request({
    url: '/app/appSlide',
    method: 'put',
    data: data
  })
}

// 删除幻灯片
export function delAppSlide (id) {
  return request({
    url: '/app/appSlide/' + id,
    method: 'delete'
  })
}

// 查询最大编号
export function findMaxSort () {
  return request({
    url: '/app/appSlide/findMaxSort',
    method: 'get'
  })
}
// 导出幻灯片
export function exportAppSlide (query) {
  return request({
    url: '/app/appSlide/export',
    method: 'get',
    params: query
  })
}
