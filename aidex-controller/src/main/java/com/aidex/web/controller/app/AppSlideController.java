package com.aidex.web.controller.app;

import com.aidex.app.domain.AppSlide;
import com.aidex.app.service.AppSlideService;
import com.aidex.common.annotation.Log;
import com.aidex.common.core.controller.BaseController;
import com.aidex.common.core.domain.R;
import com.aidex.common.core.page.PageDomain;
import com.aidex.common.enums.BusinessType;
import com.aidex.common.utils.poi.ExcelUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 幻灯片Controller
 * @author wws
 * @email 595706397@qq.com
 * @date 2021-12-20
 */
@RestController
@RequestMapping("/app/appSlide")
public class AppSlideController extends BaseController
{
    @Autowired
    private AppSlideService appSlideService;

    /**
     * 查询幻灯片列表
     */
    @PreAuthorize("@ss.hasPermi('app:appSlide:list')")
    @GetMapping("/list")
    public R<PageInfo> list(AppSlide appSlide, HttpServletRequest request, HttpServletResponse response)
    {
        appSlide.setPage(new PageDomain(request, response));
        return R.data(appSlideService.findPage(appSlide));
    }

    /**
     * 获取幻灯片详细信息
     */
    @PreAuthorize("@ss.hasPermi('app:appSlide:query')")
    @GetMapping(value = "/{id}")
    public R<AppSlide> detail(@PathVariable("id") String id)
    {
        return R.data(appSlideService.get(id));
    }

    /**
     * 新增幻灯片
     */
    @PreAuthorize("@ss.hasPermi('app:appSlide:add')")
    @Log(title = "幻灯片", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody @Validated  AppSlide appSlide)
    {
        return R.status(appSlideService.save(appSlide));
    }

    /**
     * 修改幻灯片
     */
    @PreAuthorize("@ss.hasPermi('app:appSlide:edit')")
    @Log(title = "幻灯片", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody @Validated AppSlide appSlide)
    {
        return R.status(appSlideService.save(appSlide));
    }


    /**
     * 删除幻灯片
     */
    @PreAuthorize("@ss.hasPermi('app:appSlide:remove')")
    @Log(title = "幻灯片", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R remove(@PathVariable String[] ids)
    {
        return R.status(appSlideService.deleteAppSlideByIds(ids));
    }

    /**
     * 获取最大编号
     */
    @PreAuthorize("@ss.hasPermi('app:appSlide:query')")
    @GetMapping("/findMaxSort")
    public R findMaxSort()
    {
        return R.data(appSlideService.findMaxSort(new AppSlide()));
    }

    /**
     * 导出幻灯片列表
     */
    @PreAuthorize("@ss.hasPermi('app:appSlide:export')")
    @Log(title = "幻灯片", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public R export(AppSlide appSlide)
    {
        List<AppSlide> list = appSlideService.findList(appSlide);
        ExcelUtil<AppSlide> util = new ExcelUtil<AppSlide>(AppSlide.class);
        return util.exportExcel(list, "幻灯片数据");
    }

}
